import React from 'react';
import logo from '../../../assets/images/mwgLogoWhite.png';

const Header = () => (
  <div className="header">
    <img
      className="header__logo"
      src={logo}
      alt={"My Web Grocer Logo"}
    />
  </div>
);

export default Header;
