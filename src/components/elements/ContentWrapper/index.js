import React from 'react';
import PropTypes from 'prop-types';

const ContentWrapper = ({ children, type, className }) => (
  <div className="content-wrapper">
    <div className={`content-wrapper__box  ${className} ${type ? `content-wrapper__box--${type}` : ''}`}>
      {children}
    </div>
  </div>
);

ContentWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
  className: PropTypes.string,
};

ContentWrapper.defaultProps = {
  className: '',
};

export default ContentWrapper;
