import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import check from '../../../assets/images/check.png';

export default class Input extends PureComponent{
  static propTypes = {
    type: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node,
      PropTypes.number,
    ]),
    wrapperClassName: PropTypes.string,
    labelClassName: PropTypes.string,
    inputClassName: PropTypes.string,
    defaultValue: PropTypes.string,
    placeholder: PropTypes.string,
    onBlur: PropTypes.func,
    valid: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  };

  static defaultProps = {
    inputClassName: '',
    labelClassName: '',
    wrapperClassName: '',
    type: 'text',
    placeholder: '',
    valid: false,
    error: '',
  };

  render = () => {
    const { wrapperClassName, labelClassName, inputClassName, onChange, label, value, type, defaultValue, onBlur, placeholder, error, valid } = this.props;

    return (
      <div className={`input-wrapper ${wrapperClassName}`}>
        {label && (
          <p className={`input-wrapper__label ${labelClassName}`}>{label}</p>
        )}

        <div className="input-wrapper__input-container">
          <img
            src={check}
            alt={'check'}
            className={`input-wrapper__check ${valid ? 'input-wrapper__check--visible' : ''}`}
          />

          <input
            className={`input-wrapper__input ${inputClassName}`}
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            defaultValue={defaultValue}
            type={type}
            placeholder={placeholder}
          />
        </div>

        {error && (
          <div className="input-wrapper__error">
            <span className="input-wrapper__error-intro">Error:</span>
            <span className="input-wrapper__error-text">{error}</span>

          </div>
        )}
      </div>
    );
  };
}
