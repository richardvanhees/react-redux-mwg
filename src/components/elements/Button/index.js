import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Button extends Component{
  static propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    style: PropTypes.object,
    type: PropTypes.oneOf(['light', 'dark']),
  };
  static defaultProps = {
    className: '',
    type: 'light'
  };

  render() {
    const { onClick, className, label, type } = this.props;

    return (
      <button
        className={`button ${className} button--${type}`}
        onClick={onClick}
      >
        {label}
      </button>
    );
  }
}
