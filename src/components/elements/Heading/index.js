import React from 'react';
import PropTypes from 'prop-types';

const Heading = ({ children, type }) => (
  <h1 className={`heading ${type ? `heading--${type}` : ''}`}>
    {children}
  </h1>
);

Heading.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
};

Heading.defaultProps = {
  type: '',
};

export default Heading;
