import React from 'react';
import PropTypes from 'prop-types';
import backgroundImage from '../../../assets/images/OFC16_Cafe_018_1600.png';

const Wrapper = ({ children }) => (
  <div className="wrapper">
    <img
      className="wrapper__background-image"
      src={backgroundImage}
      alt={"My Web Grocer Background"}
    />
    <div className="wrapper__content">
      {children}
    </div>
  </div>
);

Wrapper.propTypes = {
  children: PropTypes.node.isRequired
};

export default Wrapper;
