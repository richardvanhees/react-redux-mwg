import React, { PureComponent } from 'react';

import Header from '../../elements/Header';
import Wrapper from '../../elements/Wrapper';

import IntroView from '../IntroView';
import FormView from '../FormView';
import ThankYouView from '../ThankYouView';

export default class App extends PureComponent{
  render() {
    const { view } = this.props;

    return (
      <div className="app">
        <Header/>
        <Wrapper>
          {view === 'intro' && <IntroView {...this.props} />}
          {view === 'form' && <FormView {...this.props} />}
          {view === 'thank-you' && <ThankYouView {...this.props} />}
        </Wrapper>
      </div>
    );
  }
}

