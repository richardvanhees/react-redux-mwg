import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setView, submitForm } from '../../../modules/form/actions';

import AppComponent from './component';

const mapStateToProps = ({ form }) => ({
  view: form.activeView,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setView,
      submitForm,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent)
