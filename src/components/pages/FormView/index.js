import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { checkFirstName, checkSurname, checkEmail, checkDob } from '../../../modules/utils/validationRules';

import ContentWrapper from '../../elements/ContentWrapper';
import Input from '../../elements/Input';
import Button from "../../elements/Button";

export default class FormView extends Component{
  static propTypes = {
    submitForm: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      surname: '',
      dob: '',
      nationality: '',
      email: '',
      occupation: '',
      errors: {
        firstName: null,
        surname: null,
        dob: null,
        nationality: null,
        email: null,
        occupation: null,
      },
    };
  };

  setError = (key, errorText) => {
    this.setState({
      ...this.state,
      errors: {
        ...this.state.errors,
        [key]: errorText,
      },
    });
  };

  validateInput = ({ value, key, errorText, validator }) => {
    if(this.state[key].trim()){
      if(validator && !validator(value)){
        return this.setError(key, errorText);
      }
      return this.setError(key, false);
    }

    return this.setError(key, null);
  };

  checkIfAllValid = () => {
    for(let key in this.state.errors){
      if(this.state.errors[key] !== false) return false;
    }
    return true;
  };

  submit = async () => {
    !this.state.firstName.trim() && await this.setError('firstName', 'First name is required');
    !this.state.surname.trim() && await this.setError('surname', 'Surname is required');
    !this.state.dob.trim() && await this.setError('dob', 'Date of birth is required');
    !this.state.nationality.trim() && await this.setError('nationality', 'Nationality is required');
    !this.state.email.trim() && await this.setError('email', 'Email is required');
    !this.state.occupation.trim() && await this.setError('occupation', 'Occupation is required');

    if(this.checkIfAllValid()){
      this.props.submitForm(this.state);
    }
  };

  render = () =>
    <ContentWrapper className="form" type={'form'}>
      <div className="form__container">
        <Input
          wrapperClassName={"form__input-block"}
          label={"First Name"}
          placeholder={"John"}
          defaultValue={this.state.firstName}
          error={this.state.errors.firstName}
          onChange={e => this.setState({ firstName: e.target.value })}
          onBlur={e => this.validateInput({
            value: e.target.value,
            key: 'firstName',
            errorText: 'Enter a valid first name',
            validator: checkFirstName,
          })}
          valid={this.state.errors.firstName === false}
        />

        <Input
          wrapperClassName={"form__input-block"}
          label={"Surname"}
          placeholder={"Doe"}
          defaultValue={this.state.surname}
          error={this.state.errors.surname}
          onChange={e => this.setState({ surname: e.target.value })}
          onBlur={e => this.validateInput({
            value: e.target.value,
            key: 'surname',
            errorText: 'Enter a valid surname',
            validator: checkSurname,
          })}
          valid={this.state.errors.surname === false}
        />

        <Input
          wrapperClassName={"form__input-block"}
          label={"Date of Birth"}
          placeholder={"01/01/1980"}
          defaultValue={this.state.dob}
          error={this.state.errors.dob}
          onChange={e => this.setState({ dob: e.target.value })}
          onBlur={e => this.validateInput({
            value: e.target.value,
            key: 'dob',
            errorText: 'Enter a valid date of birth',
            validator: checkDob,
          })}
          valid={this.state.errors.dob === false}
        />

        <Input
          wrapperClassName={"form__input-block"}
          label={"Nationality"}
          placeholder={"Irish"}
          defaultValue={this.state.nationality}
          error={this.state.errors.nationality}
          onChange={e => this.setState({ nationality: e.target.value })}
          onBlur={e => this.validateInput({
            value: e.target.value,
            key: 'nationality',
          })}
          valid={this.state.errors.nationality === false}
        />

        <Input
          wrapperClassName={"form__input-block"}
          label={"Email Address"}
          placeholder={"hello@info.com"}
          defaultValue={this.state.email}
          error={this.state.errors.email}
          onChange={e => this.setState({ email: e.target.value })}
          onBlur={e => this.validateInput({
            value: e.target.value,
            key: 'email',
            errorText: 'Enter a valid email address',
            validator: checkEmail,
          })}
          valid={this.state.errors.email === false}
        />

        <Input
          wrapperClassName={"form__input-block"}
          label={"Occupation"}
          placeholder={"Front End Developer"}
          defaultValue={this.state.occupation}
          error={this.state.errors.occupation}
          onChange={e => this.setState({ occupation: e.target.value })}
          onBlur={e => this.validateInput({
            value: e.target.value,
            key: 'occupation',
          })}
          valid={this.state.errors.occupation === false}
        />

      </div>

      <Button
        onClick={this.submit}
        label={'Submit'}
        className="form__button"
        type={'dark'}
      />
    </ContentWrapper>;
}
