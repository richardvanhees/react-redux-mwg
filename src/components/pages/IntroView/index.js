import React from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from '../../elements/ContentWrapper';
import Heading from '../../elements/Heading';
import Button from "../../elements/Button";

const IntroView = ({ setView }) => (
  <ContentWrapper type={"intro"} className="intro">
    <Heading type={'intro'}>Welcome</Heading>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
      magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat.
    </p>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
      magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
    <Button
      onClick={() => setView('form')}
      label={'Next'}
      className="intro__button"
    />
  </ContentWrapper>
);

IntroView.propTypes = {
  setView: PropTypes.func.isRequired,
};

export default IntroView;
