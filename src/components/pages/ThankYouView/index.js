import React from 'react';

import ContentWrapper from '../../elements/ContentWrapper';
import Heading from '../../elements/Heading';

const ThankYouView = () => (
  <ContentWrapper className="thank-you" type={'thank-you'}>
    <Heading>Thank you</Heading>
  </ContentWrapper>
);

export default ThankYouView;
