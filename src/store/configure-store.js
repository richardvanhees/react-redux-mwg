import { call } from 'redux-saga/effects';
import configureStoreBoilerplate from './configure-store-boilerplate';
import formReducer, { submitFormSaga } from '../modules/form';

export const configureStore = (history, initialState = {}) =>
  configureStoreBoilerplate(
    {
      form: formReducer,
    },
    [
      call(submitFormSaga)
    ],
    history,
    initialState
  );
