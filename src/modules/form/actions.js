import formTypes from './types';

export const setView = view => ({
  type: formTypes.SET_VIEW,
  view,
});

export const submitForm = userDetails => ({
  type: formTypes.SUBMIT_FORM,
  userDetails,
});
