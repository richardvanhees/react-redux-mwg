export default {
  SET_VIEW: 'form/SET_VIEW',
  SUBMIT_FORM: 'form/SUBMIT_FORM',
};
