import { call, put, takeLatest } from 'redux-saga/effects';
import { post } from '../../utils/axios-wrapper';
import userTypes from '../types';
import { setView } from '../actions';

export const callToSubmitForm = userDetails =>
  post({ route: '/user', userDetails });

export function* submitForm({ userDetails = {} }) {
  try{
    console.log('Doing stuff with:', userDetails);
    yield call(callToSubmitForm, userDetails);
    yield put(setView('thank-you'));
  } catch(error){
    console.log(error);
  }
}

export default function* submitFormSaga() {
  yield takeLatest(userTypes.SUBMIT_FORM, submitForm);
}
