import formTypes from './types';

const initialState = {
  activeView: 'intro',
};

export const setView = (state, { view }) => ({
  ...state,
  activeView: view,
});


const reducer = {
  [formTypes.SET_VIEW]: setView,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
