import moment from 'moment';

export const checkFirstName = val => val ? (/^[A-Za-z\s\-']+$/).test(val) : null;

export const checkSurname = val => val ? (/^[A-Za-z\s\-']+$/).test(val) : null;

export const checkDob = val => val ? moment(val, 'DD/MM/YYYY', true).isValid() && moment(val).isBefore(moment()) : null;

export const checkEmail = val => val ? (/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/).test(val) : null;
