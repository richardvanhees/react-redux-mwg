### Intro

This is an example project built with React and NodeJS.

#### Setup
- Run `yarn` to install dependencies  
- Run `yarn start` to start the application

#### Team

- Richard van Hees - *developer* 